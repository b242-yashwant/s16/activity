console.log("Hello batch 242!");

// JavaScript Operators

// Arithmetic Operators

let x = 1397;
let y = 7831;

// Sum
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Difference
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Product
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Quotient
let quotient = x / y;
console.log("Result of division operator: " + quotient);

//modulus
let remainder= x%y;
console.log("Result of modulo operator:" +remainder);
// assignment operator
// basic assignment operator
let aN=8;
// addition assignment operator(+=)
aN+=2;
console.log("result of addition assignment operator:" +aN);
aN-=2;
console.log("result of substraction assignment:"+aN);
aN/=2;
console.log("result of division assignment"+ aN);
aN*=2;
console.log("result of multiplication assignment:"+aN);

//Multiple operators and parentheses
let mdas = 1+ 2-3*4/6;
console.log("mdas result:"+mdas);

// the order of operations can be changed by adding parenthesis to the logic
let pemdas= 1+ (8-3)*4/5;
console.log("pemdas result:"+ pemdas);

//increment and decrement
let z=3;
//pre-increment
let increment=++z;
console.log("increment pre-increment result:"+increment);
console.log("increment pre-increment result:"+z);
//post-increment
increment=z++;
console.log("increment post-increment result:"+increment);
console.log("increment post-increment result:"+z);
console.log(++z);
console.log(z++);
console.log(z);


//pre-decrement
let decrement=--z;
console.log("increment pre-decrement result:"+increment);
console.log("increment pre-decrement result:"+z);
//post-decrement
decrement=z--;
console.log("increment post-decrement result:"+increment);
console.log("increment post-decrement result:"+z);



let numA= '10';
let numB=12;
coercion=numA+numB;
console.log(coercion);
console.log(typeof coercion);
let numE=true+1;
console.log(numE);
console.log(typeof numE);
//comarison operators(==);
let juan='juan';

console.log(1 == 1);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log(juan == 'juan');

//inequality operator
console.log(1 != 1);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log(juan != 'juan');
//strict equality operator

console.log(1 === 1);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log(juan === 'juan');
//strict inequality operator(!==)
console.log(1 !== 1);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log(juan !== 'juan');

//rational operators
let a=50;
let b=65;

let isGreaterThan=a>b;
let isLessThan=a<b;
let isGtorEqual=a>=b;
let isLTorEqual=a<=b;
console.log("rational operators");
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGtorEqual);
console.log(isLTorEqual);
let numStr="30";
console.log("relational operator with type coercion");
console.log(a>numStr);

//logical operators
let isLegalAge=true;
let isRegistered=false;

// and operator (&&)
//returs true if all operants are true
let allRequirementsMet= isLegalAge && isRegistered;
console.log(allRequirementsMet);

// or operator(||)
let allRequirementsNotMet= isLegalAge || isRegistered;
console.log(allRequirementsNotMet);

//not operator (!)
let someRequirementsNotMet=!allRequirementsMet;
console.log(someRequirementsNotMet);














